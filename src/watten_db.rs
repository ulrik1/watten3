use modular_bitfield::prelude::*;
use std::ops::BitOr;
use std::sync::Arc;

static COMBINATIONS: [[i32; 5];120] = [
    [1,2,3,4,5],
    [2,1,3,4,5],
    [3,1,2,4,5],
    [1,3,2,4,5],
    [2,3,1,4,5],
    [3,2,1,4,5],
    [3,2,4,1,5],
    [2,3,4,1,5],
    [4,3,2,1,5],
    [3,4,2,1,5],
    [2,4,3,1,5],
    [4,2,3,1,5],
    [4,1,3,2,5],
    [1,4,3,2,5],
    [3,4,1,2,5],
    [4,3,1,2,5],
    [1,3,4,2,5],
    [3,1,4,2,5],
    [2,1,4,3,5],
    [1,2,4,3,5],
    [4,2,1,3,5],
    [2,4,1,3,5],
    [1,4,2,3,5],
    [4,1,2,3,5],
    [5,1,2,3,4],
    [1,5,2,3,4],
    [2,5,1,3,4],
    [5,2,1,3,4],
    [1,2,5,3,4],
    [2,1,5,3,4],
    [2,1,3,5,4],
    [1,2,3,5,4],
    [3,2,1,5,4],
    [2,3,1,5,4],
    [1,3,2,5,4],
    [3,1,2,5,4],
    [3,5,2,1,4],
    [5,3,2,1,4],
    [2,3,5,1,4],
    [3,2,5,1,4],
    [5,2,3,1,4],
    [2,5,3,1,4],
    [1,5,3,2,4],
    [5,1,3,2,4],
    [3,1,5,2,4],
    [1,3,5,2,4],
    [5,3,1,2,4],
    [3,5,1,2,4],
    [4,5,1,2,3],
    [5,4,1,2,3],
    [1,4,5,2,3],
    [4,1,5,2,3],
    [5,1,4,2,3],
    [1,5,4,2,3],
    [1,5,2,4,3],
    [5,1,2,4,3],
    [2,1,5,4,3],
    [1,2,5,4,3],
    [5,2,1,4,3],
    [2,5,1,4,3],
    [2,4,1,5,3],
    [4,2,1,5,3],
    [1,2,4,5,3],
    [2,1,4,5,3],
    [4,1,2,5,3],
    [1,4,2,5,3],
    [5,4,2,1,3],
    [4,5,2,1,3],
    [2,5,4,1,3],
    [5,2,4,1,3],
    [4,2,5,1,3],
    [2,4,5,1,3],
    [3,4,5,1,2],
    [4,3,5,1,2],
    [5,3,4,1,2],
    [3,5,4,1,2],
    [4,5,3,1,2],
    [5,4,3,1,2],
    [5,4,1,3,2],
    [4,5,1,3,2],
    [1,5,4,3,2],
    [5,1,4,3,2],
    [4,1,5,3,2],
    [1,4,5,3,2],
    [1,3,5,4,2],
    [3,1,5,4,2],
    [5,1,3,4,2],
    [1,5,3,4,2],
    [3,5,1,4,2],
    [5,3,1,4,2],
    [4,3,1,5,2],
    [3,4,1,5,2],
    [1,4,3,5,2],
    [4,1,3,5,2],
    [3,1,4,5,2],
    [1,3,4,5,2],
    [2,3,4,5,1],
    [3,2,4,5,1],
    [4,2,3,5,1],
    [2,4,3,5,1],
    [3,4,2,5,1],
    [4,3,2,5,1],
    [4,3,5,2,1],
    [3,4,5,2,1],
    [5,4,3,2,1],
    [4,5,3,2,1],
    [3,5,4,2,1],
    [5,3,4,2,1],
    [5,2,4,3,1],
    [2,5,4,3,1],
    [4,5,2,3,1],
    [5,4,2,3,1],
    [2,4,5,3,1],
    [4,2,5,3,1],
    [3,2,5,4,1],
    [2,3,5,4,1],
    [5,3,2,4,1],
    [3,5,2,4,1],
    [2,5,3,4,1],
    [5,2,3,4,1]
];

enum Rules {
    Followed = 0x00,
    Broken = 0x02
}

enum Winner {
    Team0 = 0x00,
    Team1 = 0x01
}

#[derive(BitfieldSpecifier)]
#[bits = 2]
enum GameResult {
    Team0RulesFollowed = 0, // Winner::Team0 | Rules::Followed,
    Team1RulesFollowed = 1, // Winner::Team1 | Rules::Followed,
    Team0RulesBroken = 2, // Winner::Team0 | Rules::Broken,
    Team1RulesBroken = 3, // Winner::Team1 | Rules::Broken
}

#[bitfield]
#[derive(BitfieldSpecifier)]
pub(crate) struct GameWinnerBlock {
    game0_winner: GameResult,
    game1_winner: GameResult,
    game2_winner: GameResult,
    game3_winner: GameResult,
}

const NUM_ITEMS: usize = 120 * 120 * 120 * 15;

lazy_static! {
    static ref INIT_WINNER_BLOCK: GameWinnerBlock = GameWinnerBlock {
        game0_winner: GameResult::Team0RulesFollowed,
        game1_winner: GameResult::Team0RulesFollowed,
        game2_winner: GameResult::Team0RulesFollowed,
        game3_winner: GameResult::Team0RulesFollowed
    };

    static ref PLAYED_GAMES: [GameWinnerBlock; NUM_ITEMS] = [INIT_WINNER_BLOCK; NUM_ITEMS];
}


static PLAYER_MULTIPLIER : [usize; 4] = [
    COMBINATIONS.len() ^ 0,
    COMBINATIONS.len() ^ 1,
    COMBINATIONS.len() ^ 2,
    COMBINATIONS.len() ^ 3
];

fn get_game_index(p0: usize, p1: usize, p2: usize, p3: usize) -> usize {
    return (
        p0 * (COMBINATIONS.len() ^ 0) +
        p1 * (COMBINATIONS.len() ^ 1) +
        p2 * (COMBINATIONS.len() ^ 2) +
        p3 * (COMBINATIONS.len() ^ 3)
    ) as usize;
}

fn set_game_result(p0: usize, p1: usize, p2: usize, p3: usize, result: GameResult) {
    let game_idx = get_game_index(p0, p1, p2, p3);
    let game_byte_idx : usize = game_idx >> 3;

//    let start_time = Arc::new(Utc::now().to_string());

    unsafe {
        let game_block = PLAYED_GAMES[game_byte_idx];
        let game_result = game_block;
        PLAYED_GAMES[game_byte_idx] = game_result;
    }
}

fn play_game(p0: usize, p1: usize, p2: usize, p3: usize) -> GameResult {
    return GameResult::Team1RulesBroken;
}

pub(crate) fn init(deck: [u8; 33]) -> u32 {
    for p0 in 0..COMBINATIONS.len() {
        for p1 in 0..COMBINATIONS.len() {
            for p2 in 0..COMBINATIONS.len() {
                for p3 in 0..COMBINATIONS.len() {
                    let winning_team = play_game(p0, p1, p2, p3);
                    set_game_result(p0, p1, p2, p3, winning_team);
                }
            }
        }
    }

    return 5;
}
