#[macro_use]
extern crate lazy_static;

mod deck;
mod watten_db;

use crate::deck::Card;

fn main() {
    let d = deck::shuffle();
    let db = watten_db::init(d);
    for x in 0..33 {
        println!("Card {0}: {1}", x, Card::from(d[x]));
    }
}
