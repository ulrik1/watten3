
use std::fmt;
use num_traits::{FromPrimitive};
use enum_primitive_derive::Primitive;

use rand::Rng;
use std::convert::From;

#[derive(Debug, Eq, PartialEq, Primitive)]
enum Value {
    Six     = 6,
    Seven   = 7,
    Eight   = 8,
    Nine    = 9,
    Ten     = 10,
    Unter   = 11,
    Ober    = 12,
    King    = 13,
    Ace     = 14
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Value::Six => write!(f, "6"),
            Value::Seven => write!(f, "7"),
            Value::Eight => write!(f, "8"),
            Value::Nine => write!(f, "9"),
            Value::Ten => write!(f, "10"),
            Value::Unter => write!(f, "Unter"),
            Value::Ober => write!(f, "Ober"),
            Value::King => write!(f, "King"),
            Value::Ace => write!(f, "Ace"),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Primitive)]
enum Suit {
    Bells = 0x00,
    Leaves = 0x10,
    Acorns = 0x20,
    Hearts = 0x30
}

impl fmt::Display for Suit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Suit::Hearts => write!(f, "Hearts"),
            Suit::Leaves => write!(f, "Leaves"),
            Suit::Bells => write!(f, "Bells"),
            Suit::Acorns => write!(f, "Acorns"),
        }
    }
}

#[derive(Debug)]
pub(crate) struct Card {
    suit: Suit,
    value: Value,
}

impl fmt::Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "{0} of {1}", self.value, self.suit)
    }
}

impl From<u8> for Card {
    fn from(item: u8) -> Self {
        let m = Suit::from_u8(item & 0xf0).unwrap_or(Suit::Bells);
        let v = Value::from_u8(item & 0x0f).unwrap_or(Value::Six);
        Card { suit: m, value: v }
    }
}

impl From<Card> for u8 {
    fn from(item: Card) -> Self {
        return item.suit as u8 | item.value as u8;
    }
}

pub(crate) fn shuffle() -> [u8; 33] {
    let mut d: [u8; 33] = [
        u8::from(Card { suit: Suit::Bells, value: Value::Six }),
        u8::from(Card { suit: Suit::Bells, value: Value::Seven }),
        u8::from(Card { suit: Suit::Bells, value: Value::Eight }),
        u8::from(Card { suit: Suit::Bells, value: Value::Nine }),
        u8::from(Card { suit: Suit::Bells, value: Value::Ten }),
        u8::from(Card { suit: Suit::Bells, value: Value::Unter }),
        u8::from(Card { suit: Suit::Bells, value: Value::Ober }),
        u8::from(Card { suit: Suit::Bells, value: Value::King }),
        u8::from(Card { suit: Suit::Bells, value: Value::Ace }),

        u8::from(Card { suit: Suit::Leaves, value: Value::Seven }),
        u8::from(Card { suit: Suit::Leaves, value: Value::Eight }),
        u8::from(Card { suit: Suit::Leaves, value: Value::Nine }),
        u8::from(Card { suit: Suit::Leaves, value: Value::Ten }),
        u8::from(Card { suit: Suit::Leaves, value: Value::Unter }),
        u8::from(Card { suit: Suit::Leaves, value: Value::Ober }),
        u8::from(Card { suit: Suit::Leaves, value: Value::King }),
        u8::from(Card { suit: Suit::Leaves, value: Value::Ace }),

        u8::from(Card { suit: Suit::Acorns, value: Value::Seven }),
        u8::from(Card { suit: Suit::Acorns, value: Value::Eight }),
        u8::from(Card { suit: Suit::Acorns, value: Value::Nine }),
        u8::from(Card { suit: Suit::Acorns, value: Value::Ten }),
        u8::from(Card { suit: Suit::Acorns, value: Value::Unter }),
        u8::from(Card { suit: Suit::Acorns, value: Value::Ober }),
        u8::from(Card { suit: Suit::Acorns, value: Value::King }),
        u8::from(Card { suit: Suit::Acorns, value: Value::Ace }),

        u8::from(Card { suit: Suit::Hearts, value: Value::Seven }),
        u8::from(Card { suit: Suit::Hearts, value: Value::Eight }),
        u8::from(Card { suit: Suit::Hearts, value: Value::Nine }),
        u8::from(Card { suit: Suit::Hearts, value: Value::Ten }),
        u8::from(Card { suit: Suit::Hearts, value: Value::Unter }),
        u8::from(Card { suit: Suit::Hearts, value: Value::Ober }),
        u8::from(Card { suit: Suit::Hearts, value: Value::King }),
        u8::from(Card { suit: Suit::Hearts, value: Value::Ace }),
    ];

    for _ in 1..1000 {
        let from = rand::thread_rng().gen_range(0..33);
        let to = rand::thread_rng().gen_range(0..33);

        let tmp = d[from];
        d[from] = d[to];
        d[to] = tmp;
    }

    return d;
}
